"use strict";

const firstNumber = prompt("Введите первое число", "");
if (!firstNumber) console.log("Первое число не указано");
const expr = prompt("Введите знак", "");
if (!firstNumber) console.log("Не введён знак");
const secondNumber = prompt("Введите второе число", "");
if (!firstNumber) console.log("Второе число не указано");

if (isNaN(+firstNumber) || isNaN(+secondNumber))
  console.log("Некорректный ввод чисел");

switch (expr) {
  case "+":
    console.log(+firstNumber + +secondNumber);
    break;
  case "-":
    console.log(+firstNumber - +secondNumber);
    break;
  case "/":
    console.log(+firstNumber / +secondNumber);
    break;
  case "*":
    console.log(+firstNumber * +secondNumber);
    break;
  default:
    console.log("Программа не поддерживает такую операцию");
}
